// ex7.cpp : Defines the entry point for the application.
//
#include "video_player.h"
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <tchar.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void CreateControls(HWND hwnd);
void UpdateLabel(void);

HWND hTrack;
HWND hlbl;
HINSTANCE hinst;
#define ID_FIRSTCHILD	100

void CreateControls(HWND hwnd) {
   hlbl = CreateWindowW(L"Static", L"0", WS_CHILD | WS_VISIBLE,
      5, 495, 30, 30, hwnd, (HMENU)3, NULL, NULL);
   HWND hRightLabel = CreateWindowW(L"Static", L"100", WS_CHILD | WS_VISIBLE,
      610, 495, 30, 30, hwnd, (HMENU)3, NULL, NULL);

   INITCOMMONCONTROLSEX icex;

   icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
   icex.dwICC = ICC_LISTVIEW_CLASSES;
   InitCommonControlsEx(&icex);

   hTrack = CreateWindowW(TRACKBAR_CLASSW, L"Trackbar Control",
      WS_CHILD | WS_VISIBLE,
      35, 495, 555, 30, hwnd, (HMENU)3, NULL, NULL);

   SendMessageW(hTrack, TBM_SETRANGE, TRUE, MAKELONG(0, 100));
   SendMessageW(hTrack, TBM_SETPOS, FALSE, 0);
   SendMessageW(hTrack, TBM_SETBUDDY, FALSE, (LPARAM)hRightLabel);
}

void UpdateLabel(void) {

   LRESULT pos = SendMessageW(hTrack, TBM_GETPOS, 0, 0);
   wchar_t buf[4];
   wsprintfW(buf, L"%ld", pos);

   SetWindowTextW(hlbl, buf);
}

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
   PWSTR lpCmdLine, int nCmdShow) {

   HWND hwnd;
   MSG  msg;

   WNDCLASSW wc = { 0 };
   wc.lpszClassName = L"Trackbar";
   wc.hInstance = hInstance;
   wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
   wc.lpfnWndProc = WndProc;
   wc.hCursor = LoadCursor(0, IDC_ARROW);

   RegisterClassW(&wc);
   hwnd = CreateWindowW(wc.lpszClassName, L"Trackbar",
      WS_OVERLAPPEDWINDOW | WS_VISIBLE, 100, 100, 640, 580, 0, 0, hInstance, 0);

   while (GetMessage(&msg, NULL, 0, 0)) {

      TranslateMessage(&msg);
      DispatchMessage(&msg);
   }

   return (int)msg.wParam;
}

LRESULT CALLBACK ChildProc(HWND hwnd, UINT Message, WPARAM wparam, LPARAM lparam)
{
   if(Message == WM_SHOWWINDOW)
      VideoPlayer::instance().PlayVideo(const_cast<char*>("sample.mov"));
   if (Message == WM_DESTROY)
   {
      return 0;
   }
   return DefWindowProc(hwnd, Message, wparam, lparam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg,
   WPARAM wParam, LPARAM lParam) {

   switch (msg) {

   case WM_CREATE:
      WNDCLASS w;
      memset(&w, 0, sizeof(WNDCLASS));
      w.lpfnWndProc = ChildProc;
      w.hInstance = hinst;
      w.hbrBackground = GetStockBrush(WHITE_BRUSH);
      w.lpszClassName = _T("ChildWClass");
      w.hCursor = LoadCursor(NULL, IDC_CROSS);
      RegisterClass(&w);
      HWND child;
      child = CreateWindowEx(0, _T("ChildWClass"), (LPCTSTR)NULL,
         WS_CHILD | WS_BORDER | WS_VISIBLE, 10, 10,
         640, 480, hwnd, (HMENU)(int)(ID_FIRSTCHILD), hinst, NULL);
      ShowWindow(child, SW_NORMAL);
      UpdateWindow(child);
      CreateControls(hwnd);
      break;

   case WM_HSCROLL:
      UpdateLabel();
      break;

   case WM_DESTROY:
      PostQuitMessage(0);
      break;
   }

   return DefWindowProcW(hwnd, msg, wParam, lParam);
}

