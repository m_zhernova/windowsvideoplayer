#pragma once
extern "C"
{
#include <libavformat/avformat.h>
}
#include <Windows.h>
#include <mutex>
#include <condition_variable>
#define MAX_AUDIOQ_SIZE (5 * 16 * 1024)
#define MAX_AUDIO_FRAME_SIZE 192000
#define VIDEO_PICTURE_QUEUE_SIZE 1

class SDL_Renderer;
class SDL_Window;
class SDL_Texture;
class SDL_mutex;
class SDL_Overlay;
class SDL_cond;
class SDL_Thread;

typedef struct PacketQueue
{
   AVPacketList* firstPacket, * lastPacket;
   int packetsNum;
   int size;
   //SDL_mutex* mutex;
   std::mutex mutex;
   std::condition_variable condition;
   //SDL_cond* condition;
} PacketQueue;

typedef struct VideoPicture
{
   SDL_Texture* overlay;
   int width, height;
   int allocated;
   double pts;
} VideoPicture;

typedef struct AudioStreamInfo
{
   int    audioStreamInd;
   AVStream* audioStream;
   AVCodecContext* audioContext;
   AVFrame  audioFrame;
   double          audioClock;
}AudioStreamInfo;

typedef struct AudioBufferInfo
{
   uint8_t  audioBuffer[(MAX_AUDIO_FRAME_SIZE * 3) / 2];
   unsigned int   audioBufferSize;
   unsigned int   audioBufferIndex;
}AudioBufferInfo;

typedef struct AudioPacketInfo
{
   AVPacket audioPacket;
   uint8_t* audioPacketData;
   int   audioPacketSize;
}AudioPacketInfo;

typedef struct VideoStreamInfo
{
   int   videoStreamInd;
   AVStream* videoStream;
   AVCodecContext* videoContext;
   double          videoClock;
}VideoStreamInfo;

typedef struct PictQueueInfo
{
   VideoPicture   pictQueue[VIDEO_PICTURE_QUEUE_SIZE];
   int   pictQueueSize, pictQueueReadIndex, pictQueueWriteIndex;
   SDL_mutex* pictQueueMutex;
   SDL_cond* pictQueueCondition;
}PictQueueInfo;

typedef struct VideoPts
{
   double          videoCurrentPts;
   int64_t         videoCurrentPtsTime;
}VideoPts;

typedef struct SyncInfo
{
   int             avSyncType;
   double          audioDiffCum;
   double          audioDiffAvgCoef;
   double          audioDiffThreshold;
   int             audioDiffAvgCount;
}SyncInfo;

typedef struct FrameTimerInfo
{
   double          frameTimer;
   double          frameLastPts;
   double          frameLastDelay;
}FrameTimerInfo;

typedef struct VideoThreadData
{
   VideoStreamInfo* videoStreamInfo;
   PictQueueInfo* pictQueueInfo;
   PacketQueue videoq;
   struct SwsContext* swsContext;
};

typedef struct VideoState
{
   AudioStreamInfo* audioStreamInfo;
   AudioBufferInfo* audioBufferInfo;
   AudioPacketInfo* audioPacketInfo;
   VideoThreadData* videoThreadData;
   VideoPts* videoPts;
   SyncInfo* syncInfo;
   FrameTimerInfo* frameTimerInfo;

   AVFormatContext* pFormatCtx;
   PacketQueue audioq;
   SDL_Thread* parseThread;
   SDL_Thread* videoThread;
   char            fileName[1024];
} VideoState;

enum
{
   e_AV_SYNC_AUDIO_MASTER,
   e_AV_SYNC_VIDEO_MASTER,
   e_AV_SYNC_EXTERNAL_MASTER,
};

class VideoPlayer
{
private:
   SDL_Window* m_screen;
   SDL_Renderer* m_renderer;
 //  SDL_Surface* m_screen;
   SDL_mutex* m_screenMutex;
   VideoState* m_videoState;

   int  m_quit;

   double GetVideoClock(VideoPts* videoPts);
   double GetAudioClock(AudioStreamInfo* audioStreamInfo, AudioBufferInfo* audioBufferInfo);
   double GetExternalClock();
   double GetMasterClock(VideoState* videoState);
   int SynchronizeAudio(VideoState* videoState, short* samples, int samplesSize, double pts);
   double SynchronizeVideo(VideoStreamInfo* videoStreamInfo, AVFrame* srcFrame, double pts);
   int Decode(AVCodecContext* codecContext, AVFrame* frame, bool* gotFrame, AVPacket* packet);
   int Resample(uint8_t** srcData, uint8_t** dstData, int srcSamplesNmb, int dstSamplesNmb, int64_t srcChannelLayout,
      int64_t srcRate, AVSampleFormat srcSampleFmt, AVSampleFormat dstSampleFmt, int dstChannelsNmb);

   void PacketQueueInit(PacketQueue* queue);
   int PacketQueuePut(PacketQueue* queue, AVPacket* packet);
   static int PacketQueueGet(PacketQueue* queue, AVPacket* packet, int block);
   int AudioDecodeFrame(AudioPacketInfo* audioPacketInfo, AudioStreamInfo* audioStreamInfo, PacketQueue* audioq,
      uint8_t* audioBuffer, int bufferSize, double* pts_ptr);

   static unsigned _int32 RefreshTimer(unsigned _int32 interval, void* opaque);
   static void ScheduleRefresh(VideoState* videoState, int delay);
   void VideoDisplay(VideoThreadData* videoThreadData);
   int QueuePicture(VideoThreadData* videoThreadData, AVFrame* frame, double pts);
   int OpenStreamComponent(VideoState* videoState, int streamIndex);
   static void AudioCallback(void* userdata, unsigned _int8* stream, int len);
   void VideoRefreshTimer(void* userdata);
   void AllocPicture(void* userdata);
   static int VideoThread(void* arg);
   static int DecodeThread(void* arg);
   VideoPlayer();
public:
   static VideoPlayer& instance()
   {
      static VideoPlayer inst;
      return inst;
   }
   ~VideoPlayer();
   int PlayVideo(char* fileName);
   VideoPlayer(const VideoPlayer&) = delete;
   VideoPlayer& operator = (const VideoPlayer&) = delete;
};

