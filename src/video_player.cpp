#include "video_player.h"
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#include <libavutil/channel_layout.h>
#include <libavutil/samplefmt.h>
#include <libswresample/swresample.h>
#include <libavutil/time.h>
}

#include <SDL2/SDL.h>
#include <SDL2/SDL_thread.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <Windows.h>

#define FF_REFRESH_EVENT (SDL_USEREVENT)
#define FF_QUIT_EVENT (SDL_USEREVENT + 1)
#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)
#define AV_SYNC_THRESHOLD 0.01
#define AV_NOSYNC_THRESHOLD 10.0
#define DEFAULT_AV_SYNC_TYPE e_AV_SYNC_VIDEO_MASTER
#define AUDIO_DIFF_AVG_NB 20
#define SAMPLE_CORRECTION_PERCENT_MAX 10
#define SDL_AUDIO_BUFFER_SIZE 1024

double VideoPlayer::GetVideoClock(VideoPts* videoPts)
{
   double delta = (av_gettime() - videoPts->videoCurrentPtsTime) / 1000000.0;
   return videoPts->videoCurrentPts + delta;
}
double VideoPlayer::GetAudioClock(AudioStreamInfo* audioStreamInfo, AudioBufferInfo* audioBufferInfo)
{
   double pts;
   int bufSize;
   pts = audioStreamInfo->audioClock; /* maintained in the audio thread */
   bufSize = audioBufferInfo->audioBufferSize - audioBufferInfo->audioBufferIndex;
   int bytesPerSec = 0;
   if (audioStreamInfo->audioStream)
      bytesPerSec = audioStreamInfo->audioContext->sample_rate * audioStreamInfo->audioContext->channels * 2;
   if (bytesPerSec)
      pts -= (double)bufSize / bytesPerSec;
   return pts;
}
double VideoPlayer::GetExternalClock()
{
   return av_gettime() / 1000000.0;
}
double VideoPlayer::GetMasterClock(VideoState* videoState)
{
   switch (videoState->syncInfo->avSyncType)
   {
   case e_AV_SYNC_VIDEO_MASTER:
      return GetVideoClock(videoState->videoPts);
   case e_AV_SYNC_AUDIO_MASTER:
      return GetAudioClock(videoState->audioStreamInfo, videoState->audioBufferInfo);
   case e_AV_SYNC_EXTERNAL_MASTER:
      return GetExternalClock();
   default:
      return 0;
   }
}
int VideoPlayer::SynchronizeAudio(VideoState* videoState, short* samples, int samplesSize, double pts)
{
   double refClock;
   if (videoState->syncInfo->avSyncType != e_AV_SYNC_AUDIO_MASTER)
   {
      double diff, avgDiff;
      int wantedSize, minSize, maxSize, nbSamples;
      refClock = GetMasterClock(videoState);
      diff = GetAudioClock(videoState->audioStreamInfo, videoState->audioBufferInfo) - refClock;

      if (diff < AV_NOSYNC_THRESHOLD)
      {
         videoState->syncInfo->audioDiffCum = diff + videoState->syncInfo->audioDiffAvgCoef
            * videoState->syncInfo->audioDiffCum;
         if (videoState->syncInfo->audioDiffAvgCount < AUDIO_DIFF_AVG_NB)
            videoState->syncInfo->audioDiffAvgCount++;
         else
         {
            avgDiff = videoState->syncInfo->audioDiffCum * (1.0 - videoState->syncInfo->audioDiffAvgCoef);
            if (fabs(avgDiff) >= videoState->syncInfo->audioDiffThreshold)
            {
               wantedSize = samplesSize + ((int)(diff * videoState->audioStreamInfo->audioContext->sample_rate) * 2 * videoState->audioStreamInfo->audioContext->channels);
               minSize = samplesSize * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
               maxSize = samplesSize * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);
               if (wantedSize < minSize)
                  wantedSize = minSize;
               else if (wantedSize > maxSize)
                  wantedSize = maxSize;

               if (wantedSize < samplesSize)
                  samplesSize = wantedSize;
               else if (wantedSize > samplesSize)
               {
                  uint8_t* samplesEnd, * q;
                  int nb;
                  nb = (samplesSize - wantedSize);
                  samplesEnd = (uint8_t*)samples + samplesSize - 2 * videoState->audioStreamInfo->audioContext->channels;
                  q = samplesEnd + 2 * videoState->audioStreamInfo->audioContext->channels;
                  while (nb > 0)
                  {
                     memcpy(q, samplesEnd, 2 * videoState->audioStreamInfo->audioContext->channels);
                     q += 2 * videoState->audioStreamInfo->audioContext->channels;
                     nb -= 2 * videoState->audioStreamInfo->audioContext->channels;
                  }
                  samplesSize = wantedSize;
               }
            }
         }
      }
      else
      {
         videoState->syncInfo->audioDiffAvgCount = 0;
         videoState->syncInfo->audioDiffCum = 0;
      }
   }
   return samplesSize;
}
double VideoPlayer::SynchronizeVideo(VideoStreamInfo* videoStreamInfo, AVFrame* srcFrame, double pts)
{
   double frameDelay;
   if (pts != 0)
      videoStreamInfo->videoClock = pts;
   else
      pts = videoStreamInfo->videoClock;
   frameDelay = av_q2d(videoStreamInfo->videoStream->time_base);
   frameDelay += srcFrame->repeat_pict * (frameDelay * 0.5);
   videoStreamInfo->videoClock += frameDelay;
   return pts;
}
int VideoPlayer::Decode(AVCodecContext* codecContext, AVFrame* frame, bool* gotFrame, AVPacket* packet)
{
   int error;
   *gotFrame = false;
   if (packet)
   {
      error = avcodec_send_packet(codecContext, packet);
      if (error < 0)
         return error == AVERROR_EOF ? 0 : error;
   }
   error = avcodec_receive_frame(codecContext, frame);
   if (error < 0 && error != AVERROR(EAGAIN) && error != AVERROR_EOF)
      return error;
   if (error >= 0)
      * gotFrame = true;
   return 0;
}
int VideoPlayer::Resample(uint8_t** srcData, uint8_t** dstData, int srcSamplesNmb, int dstSamplesNmb, int64_t srcChannelLayout, int64_t srcRate, AVSampleFormat srcSampleFmt, AVSampleFormat dstSampleFmt, int dstChannelsNmb)
{
   struct SwrContext* swrContext;
   swrContext = swr_alloc();
   if (!swrContext)
   {
      fprintf(stderr, "Could not allocate resampler context\n");
      int ret = AVERROR(ENOMEM);
      swr_free(&swrContext);
      return ret < 0;
   }
   av_opt_set_int(swrContext, "in_channel_layout", srcChannelLayout, 0);
   av_opt_set_int(swrContext, "in_sample_rate", srcRate, 0);
   av_opt_set_sample_fmt(swrContext, "in_sample_fmt", srcSampleFmt, 0);
   av_opt_set_int(swrContext, "out_channel_layout", srcChannelLayout, 0);
   av_opt_set_int(swrContext, "out_sample_rate", srcRate, 0);
   av_opt_set_sample_fmt(swrContext, "out_sample_fmt", dstSampleFmt, 0);
   int ret = swr_init(swrContext);
   if (ret < 0)
   {
      fprintf(stderr, "Failed to initialize the resampling context\n");
      swr_free(&swrContext);
      return ret;
   }
   ret = swr_convert(swrContext, dstData, dstSamplesNmb, (const uint8_t * *)srcData, srcSamplesNmb);
   if (ret < 0)
   {
      fprintf(stderr, "Error while converting\n");
      swr_free(&swrContext);
      return ret < 0;
   }
   int dstLinesize;
   int dstBufferSize = av_samples_get_buffer_size(&dstLinesize, dstChannelsNmb,
      ret, dstSampleFmt, 1);
   if (dstBufferSize < 0)
   {
      fprintf(stderr, "Could not get sample buffer size\n");
      swr_free(&swrContext);
      return ret < 0;
   }
   swr_free(&swrContext);
   return dstBufferSize;
}
void VideoPlayer::PacketQueueInit(PacketQueue* queue)
{
   memset(queue, 0, sizeof(PacketQueue));
   //queue->mutex = SDL_CreateMutex();
   //SDL_UnlockMutex(queue->mutex);
   
   
   //queue->condition = SDL_CreateCond();
}
int VideoPlayer::PacketQueuePut(PacketQueue* queue, AVPacket* packet)
{
   AVPacketList* pkt1;
   pkt1 = static_cast<AVPacketList*>(av_malloc(sizeof(AVPacketList)));
   if (!pkt1)
      return -1;
   pkt1->pkt = *packet;
   pkt1->next = NULL;
   queue->mutex.lock();
   //if (SDL_TryLockMutex(queue->mutex) == 0)
   {
      if (!queue->lastPacket)
         queue->firstPacket = pkt1;
      else
         queue->lastPacket->next = pkt1;
      queue->lastPacket = pkt1;
      queue->packetsNum++;
      queue->size += pkt1->pkt.size;
      //SDL_CondSignal(queue->condition);
      queue->mutex.unlock();
      queue->condition.notify_all();
      //SDL_UnlockMutex(queue->mutex);
   }
   return 0;
}
int VideoPlayer::PacketQueueGet(PacketQueue* queue, AVPacket* packet, int block)
{
   AVPacketList* packetList;
   int ret = -1;
   queue->mutex.lock();
   //if (SDL_TryLockMutex(queue->mutex) == 0)
   {
      for (;;)
      {
         if (instance().m_quit)
         {
            ret = -1;
            break;
         }
         packetList = queue->firstPacket;
         if (packetList)
         {
            queue->firstPacket = packetList->next;
            if (!queue->firstPacket)
               queue->lastPacket = NULL;
            queue->packetsNum--;
            queue->size -= packetList->pkt.size;
            *packet = packetList->pkt;
            av_free(packetList);
            ret = 1;
            break;
         }
         else if (!block)
         {
            ret = 0;
            break;
         }
         else
         {
            std::unique_lock<std::mutex> lk(queue->mutex);
            queue->condition.wait(lk);
         //SDL_CondWait(queue->condition, queue->mutex);
         }
      }
      queue->mutex.unlock();
      //SDL_UnlockMutex(queue->mutex);
   }
   return ret;
}
int VideoPlayer::AudioDecodeFrame(AudioPacketInfo* audioPacketInfo, AudioStreamInfo* audioStreamInfo, PacketQueue* audioq, uint8_t* audioBuffer, int bufferSize, double* pts_ptr)
{
   int ret, dataSize = 0;
   AVPacket* packet = av_packet_alloc();
   double pts;
   if (av_packet_ref(packet, &audioPacketInfo->audioPacket) < 0)
   {
      av_free(packet);
      return -1;
   }
   for (;;)
   {
      while (audioPacketInfo->audioPacketSize > 0)
      {
         bool gotFrame = 0;
         ret = Decode(audioStreamInfo->audioContext, &audioStreamInfo->audioFrame, &gotFrame, packet);
         if (ret < 0)
         {
            audioPacketInfo->audioPacketSize = 0;
            break;
         }
         dataSize = 0;
         if (gotFrame)
         {
            av_samples_get_buffer_size(NULL,
               audioStreamInfo->audioContext->channels,
               audioStreamInfo->audioFrame.nb_samples,
               audioStreamInfo->audioContext->sample_fmt,
               1);
            uint8_t** dstData = NULL;
            int dstLinesize;
            ret = av_samples_alloc_array_and_samples(
               &dstData,
               &dstLinesize,
               audioStreamInfo->audioContext->channels,
               SDL_AUDIO_BUFFER_SIZE,
               AVSampleFormat::AV_SAMPLE_FMT_S16,
               0);
            if (ret < 0)
            {
               fprintf(stderr, "Could not allocate destination samples\n");
               if (dstData)
                  av_freep(&dstData[0]);
               av_freep(&dstData);
               return ret;
            }
            dataSize = Resample((uint8_t * *)audioStreamInfo->audioFrame.data,
               (uint8_t * *)dstData,
               SDL_AUDIO_BUFFER_SIZE,
               SDL_AUDIO_BUFFER_SIZE,
               audioStreamInfo->audioContext->channel_layout,
               audioStreamInfo->audioContext->sample_rate,
               audioStreamInfo->audioContext->sample_fmt,
               AVSampleFormat::AV_SAMPLE_FMT_S16,
               audioStreamInfo->audioContext->channels);
            assert(dataSize <= bufferSize);
            memcpy(audioBuffer, dstData[0], dataSize);
            if (dstData)
               av_freep(&dstData[0]);
            av_freep(&dstData);
         }
         audioPacketInfo->audioPacketData += audioStreamInfo->audioFrame.pkt_size;
         audioPacketInfo->audioPacketSize -= audioStreamInfo->audioFrame.pkt_size;
         if (dataSize <= 0)
            continue;
         /////////
         pts = audioStreamInfo->audioClock;
         *pts_ptr = pts;
         audioStreamInfo->audioClock += static_cast<double>(dataSize /
            static_cast<double>(2 * audioStreamInfo->audioContext->channels * audioStreamInfo->audioContext->sample_rate));
         return dataSize;
      }
      if (m_quit)
         return -1;
      if (PacketQueueGet(audioq, packet, 1) < 0)
         return -1;
      audioPacketInfo->audioPacketData = packet->data;
      audioPacketInfo->audioPacketSize = packet->size;
      if (packet->pts != AV_NOPTS_VALUE) {
         audioStreamInfo->audioClock = av_q2d(audioStreamInfo->audioStream->time_base) * packet->pts;
      }
   }
}
unsigned _int32 VideoPlayer::RefreshTimer(unsigned _int32 interval, void* opaque)
{
   SDL_Event event;
   event.type = FF_REFRESH_EVENT;
   event.user.data1 = opaque;
   SDL_PushEvent(&event);
   return 0;
}
void VideoPlayer::ScheduleRefresh(VideoState* videoState, int delay)
{
   SDL_AddTimer(delay, RefreshTimer, videoState);
}
void VideoPlayer::VideoDisplay(VideoThreadData* videoThreadData)
{
   VideoPicture* videoPicture = &videoThreadData->pictQueueInfo->pictQueue[videoThreadData->pictQueueInfo->pictQueueReadIndex];
   if (videoPicture->overlay)
   {
      float aspect_ratio;
      if (videoThreadData->videoStreamInfo->videoContext->sample_aspect_ratio.num == 0)
         aspect_ratio = 0;
      else
         aspect_ratio = av_q2d(videoThreadData->videoStreamInfo->videoContext->sample_aspect_ratio) * videoThreadData->videoStreamInfo->videoContext->width / videoThreadData->videoStreamInfo->videoContext->height;
      if (aspect_ratio <= 0.0)
         aspect_ratio = (float)videoThreadData->videoStreamInfo->videoContext->width / (float)videoThreadData->videoStreamInfo->videoContext->height;
      int h = 480;
      int w = (static_cast<int>(rint((double)h * aspect_ratio))) & -3;
      if (w > 640)
      {
         w = 640;
         h = ((int)rint(w / aspect_ratio)) & -3;
      }
      int x = (640 - w) / 2;
      int y = (480 - h) / 2;
      SDL_Rect rect;
      rect.x = x;
      rect.y = y;
      rect.w = w;
      rect.h = h;
      SDL_LockMutex(m_screenMutex);
      SDL_RenderClear(m_renderer);
      int retcopy = SDL_RenderCopy(m_renderer, videoPicture->overlay, &rect, NULL);
      SDL_RenderPresent(m_renderer);
   //   SDL_DisplayYUVOverlay(videoPicture->overlay, &rect);
      SDL_UnlockMutex(m_screenMutex);
   }
}
int VideoPlayer::QueuePicture(VideoThreadData* videoThreadData, AVFrame* frame, double pts)
{
   VideoPicture* videoPicture;
   int dstPixelFormat;
   AVFrame picture;
   SDL_LockMutex(videoThreadData->pictQueueInfo->pictQueueMutex);
   while (videoThreadData->pictQueueInfo->pictQueueSize >= VIDEO_PICTURE_QUEUE_SIZE &&
      !m_quit)
      SDL_CondWait(videoThreadData->pictQueueInfo->pictQueueCondition, videoThreadData->pictQueueInfo->pictQueueMutex);
   SDL_UnlockMutex(videoThreadData->pictQueueInfo->pictQueueMutex);
   if (m_quit)
      return -1;
   videoPicture = &videoThreadData->pictQueueInfo->pictQueue[videoThreadData->pictQueueInfo->pictQueueWriteIndex];
   if (!videoPicture->overlay ||
      videoPicture->width != videoThreadData->videoStreamInfo->videoContext->width ||
      videoPicture->height != videoThreadData->videoStreamInfo->videoContext->height)
   {
      videoPicture->allocated = 0;
      AllocPicture(videoThreadData);
      if (m_quit)
         return -1;
   }

   if (videoPicture->overlay)
   {
      //SDL_LockYUVOverlay(videoPicture->overlay);
      dstPixelFormat = AV_PIX_FMT_YUV420P;

      /*picture.data[0] = videoPicture->overlay->pixels[0];
      picture.data[1] = videoPicture->overlay->pixels[2];
      picture.data[2] = videoPicture->overlay->pixels[1];

      picture.linesize[0] = videoPicture->overlay->pitches[0];
      picture.linesize[1] = videoPicture->overlay->pitches[2];
      picture.linesize[2] = videoPicture->overlay->pitches[1];
*/
      sws_scale(videoThreadData->swsContext,
         (uint8_t const* const*)frame->data,
         frame->linesize,
         0,
         videoThreadData->videoStreamInfo->videoContext->height,
         picture.data,
         picture.linesize);
      videoPicture->pts = pts;
    //  SDL_UnlockYUVOverlay(videoPicture->overlay);*/
      SDL_UpdateYUVTexture(videoPicture->overlay, NULL, frame->data[0], frame->linesize[0], \
         frame->data[1], frame->linesize[1], \
         frame->data[2], frame->linesize[2]);
      /*if ((start != 0) && ((std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) < 1000 / (double)videoFPS))
      {
         SDL_Delay((1000 / (double)videoFPS) - (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000));
      }*/
     
      if (++videoThreadData->pictQueueInfo->pictQueueWriteIndex == VIDEO_PICTURE_QUEUE_SIZE)
      {
         videoThreadData->pictQueueInfo->pictQueueWriteIndex = 0;
      }
      SDL_LockMutex(videoThreadData->pictQueueInfo->pictQueueMutex);
      videoThreadData->pictQueueInfo->pictQueueSize++;
      SDL_UnlockMutex(videoThreadData->pictQueueInfo->pictQueueMutex);
   }
   return 0;
}
int VideoPlayer::OpenStreamComponent(VideoState* videoState, int streamIndex)
{
   AVFormatContext* formatCtx = videoState->pFormatCtx;
   AVCodecContext* codecCtx = NULL;
   SDL_AudioSpec wantedSpec, spec;

   if (streamIndex < 0 || streamIndex >= formatCtx->nb_streams)
      return -1;
   AVCodec* codec = avcodec_find_decoder(formatCtx->streams[streamIndex]->codecpar->codec_id);
   if (!codec)
   {
      fprintf(stderr, "Unsupported codec!\n");
      return -1;
   }
   codecCtx = avcodec_alloc_context3(codec);
   if (!codecCtx)
      avformat_close_input(&formatCtx);
   int result = avcodec_parameters_to_context(
      codecCtx,
      formatCtx->streams[streamIndex]->codecpar
   );
   if (result < 0)
   {
      avformat_close_input(&formatCtx);
      avcodec_free_context(&codecCtx);
   }

   if (codecCtx->codec_type == AVMEDIA_TYPE_AUDIO)
   {
      wantedSpec.freq = codecCtx->sample_rate;
      wantedSpec.format = AUDIO_S16SYS;
      wantedSpec.channels = codecCtx->channels;
      wantedSpec.silence = 0;
      wantedSpec.samples = SDL_AUDIO_BUFFER_SIZE;
      wantedSpec.callback = AudioCallback;
      wantedSpec.userdata = videoState;

      if (SDL_OpenAudio(&wantedSpec, &spec) < 0)
      {
         fprintf(stderr, "SDL_OpenAudio: %s\n", SDL_GetError());
         return -1;
      }
   }
   if (avcodec_open2(codecCtx, codec, NULL) < 0)
   {
      fprintf(stderr, "Unsupported codec!\n");
      return -1;
   }

   switch (codecCtx->codec_type)
   {
   case AVMEDIA_TYPE_AUDIO:
      videoState->audioStreamInfo->audioStreamInd = streamIndex;
      videoState->audioStreamInfo->audioStream = formatCtx->streams[streamIndex];
      videoState->audioStreamInfo->audioContext = codecCtx;
      videoState->audioBufferInfo->audioBufferSize = 0;
      videoState->audioBufferInfo->audioBufferIndex = 0;
      memset(&videoState->audioPacketInfo->audioPacket, 0, sizeof(videoState->audioPacketInfo->audioPacket));
      PacketQueueInit(&videoState->audioq);
      SDL_PauseAudio(0);
      break;
   case AVMEDIA_TYPE_VIDEO:
      videoState->videoThreadData->videoStreamInfo->videoStreamInd = streamIndex;
      videoState->videoThreadData->videoStreamInfo->videoStream = formatCtx->streams[streamIndex];
      videoState->videoThreadData->videoStreamInfo->videoContext = codecCtx;
      videoState->frameTimerInfo->frameTimer = (double)av_gettime() / 1000000.0;
      videoState->frameTimerInfo->frameLastDelay = 40e-3;
      videoState->videoPts->videoCurrentPtsTime = av_gettime();
      PacketQueueInit(&videoState->videoThreadData->videoq);
      videoState->videoThreadData->swsContext = sws_getContext(videoState->videoThreadData->videoStreamInfo->videoContext->width,
         videoState->videoThreadData->videoStreamInfo->videoContext->height,
         videoState->videoThreadData->videoStreamInfo->videoContext->pix_fmt,
         videoState->videoThreadData->videoStreamInfo->videoContext->width,
         videoState->videoThreadData->videoStreamInfo->videoContext->height,
         AV_PIX_FMT_YUV420P,
         SWS_BILINEAR,
         NULL,
         NULL,
         NULL
      );
      videoState->videoThread = SDL_CreateThread(VideoThread, (char*)videoState->videoThreadData, (void*)"");
      break;
   default:
      break;
   }
}
void VideoPlayer::AudioCallback(void* userdata, unsigned _int8* stream, int len)
{
   VideoState* videoState = (VideoState*)userdata;
   int toWriteSize, audioSize;
   double pts;
   while (len > 0)
   {
      if (videoState->audioBufferInfo->audioBufferIndex >= videoState->audioBufferInfo->audioBufferSize)
      {
         audioSize = instance().AudioDecodeFrame(videoState->audioPacketInfo, videoState->audioStreamInfo, &videoState->audioq, videoState->audioBufferInfo->audioBuffer, sizeof(videoState->audioBufferInfo->audioBuffer), &pts);
         if (audioSize < 0)
         {
            videoState->audioBufferInfo->audioBufferSize = 1024;
            memset(videoState->audioBufferInfo->audioBuffer, 0, videoState->audioBufferInfo->audioBufferSize);
         }
         else
         {
            audioSize = instance().SynchronizeAudio(videoState, (int16_t*)videoState->audioBufferInfo->audioBuffer,
               audioSize, pts);
            videoState->audioBufferInfo->audioBufferSize = audioSize;
         }
         videoState->audioBufferInfo->audioBufferIndex = 0;
      }
      toWriteSize = videoState->audioBufferInfo->audioBufferSize - videoState->audioBufferInfo->audioBufferIndex;
      if (toWriteSize > len)
         toWriteSize = len;
      memcpy(stream, (uint8_t*)videoState->audioBufferInfo->audioBuffer + videoState->audioBufferInfo->audioBufferIndex, toWriteSize);
      len -= toWriteSize;
      stream += toWriteSize;
      videoState->audioBufferInfo->audioBufferIndex += toWriteSize;
   }
}
void VideoPlayer::VideoRefreshTimer(void* userdata)
{
   VideoState* videoState = (VideoState*)userdata;
   VideoPicture* videoPicture;
   double actualDelay, delay, syncThreshold, reClock, diff;//////////
   if (videoState->videoThreadData->videoStreamInfo->videoStream)
   {
      if (videoState->videoThreadData->pictQueueInfo->pictQueueSize == 0)
         ScheduleRefresh(videoState, 1);
      else
      {
         videoPicture = &videoState->videoThreadData->pictQueueInfo->pictQueue[videoState->videoThreadData->pictQueueInfo->pictQueueReadIndex];
         /* Now, normally here goes a ton of code
       about timing, etc. we're just going to
       guess at a delay for now. You can
       increase and decrease this value and hard code
       the timing - but I don't suggest that ;)
       We'll learn how to do it for real later.
         */
         videoState->videoPts->videoCurrentPts = videoPicture->pts;
         videoState->videoPts->videoCurrentPtsTime = av_gettime();
         delay = videoPicture->pts - videoState->frameTimerInfo->frameLastPts; /* the pts from last time */
         if (delay <= 0 || delay >= 1.0)
            delay = videoState->frameTimerInfo->frameLastDelay;
         videoState->frameTimerInfo->frameLastDelay = delay;
         videoState->frameTimerInfo->frameLastPts = videoPicture->pts;
         if (videoState->syncInfo->avSyncType != e_AV_SYNC_VIDEO_MASTER)
         {
            reClock = GetMasterClock(videoState);
            diff = videoPicture->pts - reClock;
            /* Skip or repeat the frame. Take delay into account
          FFPlay still doesn't "know if this is the best guess." */
            syncThreshold = (delay > AV_SYNC_THRESHOLD) ? delay : AV_SYNC_THRESHOLD;
            if (fabs(diff) < AV_NOSYNC_THRESHOLD)
            {
               if (diff <= -syncThreshold)
                  delay = 0;
               else if (diff >= syncThreshold)
                  delay = 2 * delay;
            }
         }
         videoState->frameTimerInfo->frameTimer += delay;
         actualDelay = videoState->frameTimerInfo->frameTimer - (av_gettime() / 1000000.0);
         if (actualDelay < 0.010)
            actualDelay = 0.010;
         ScheduleRefresh(videoState, (int)(actualDelay * 1000 + 0.5));
         VideoDisplay(videoState->videoThreadData);
         if (++videoState->videoThreadData->pictQueueInfo->pictQueueReadIndex == VIDEO_PICTURE_QUEUE_SIZE)
            videoState->videoThreadData->pictQueueInfo->pictQueueReadIndex = 0;
         SDL_LockMutex(videoState->videoThreadData->pictQueueInfo->pictQueueMutex);
         videoState->videoThreadData->pictQueueInfo->pictQueueSize--;
         SDL_CondSignal(videoState->videoThreadData->pictQueueInfo->pictQueueCondition);
         SDL_UnlockMutex(videoState->videoThreadData->pictQueueInfo->pictQueueMutex);
      }
   }
   else
      ScheduleRefresh(videoState, 100);
}
void VideoPlayer::AllocPicture(void* userdata)
{
   VideoThreadData* videoThreadData = (VideoThreadData*)userdata;
   VideoPicture* videoPicture;
   videoPicture = &videoThreadData->pictQueueInfo->pictQueue[videoThreadData->pictQueueInfo->pictQueueWriteIndex];
   SDL_LockMutex(m_screenMutex);
   videoPicture->overlay = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING,640, 480);
   SDL_UnlockMutex(m_screenMutex);
   videoPicture->width = videoThreadData->videoStreamInfo->videoContext->width;
   videoPicture->height = videoThreadData->videoStreamInfo->videoContext->height;
   videoPicture->allocated = 1;
}
int VideoPlayer::VideoThread(void* arg)
{
   VideoThreadData* videoThreadData = (VideoThreadData*)arg;
   AVPacket* packet = av_packet_alloc();
   bool frameFinished;
   double pts;
   for (;;)
   {
      if (PacketQueueGet(&videoThreadData->videoq, packet, 1) < 0)
         break;
      pts = 0;
      AVFrame* frame = av_frame_alloc();
      instance().Decode(videoThreadData->videoStreamInfo->videoContext, frame, &frameFinished, packet);
      if (packet->dts != AV_NOPTS_VALUE) {
         pts = frame->best_effort_timestamp;
      }
      else {
         pts = 0;
      }
      pts *= av_q2d(videoThreadData->videoStreamInfo->videoStream->time_base);
      if (frameFinished)
      {
         pts = instance().SynchronizeVideo(videoThreadData->videoStreamInfo, frame, pts);
         if (instance().QueuePicture(videoThreadData, frame, pts) < 0)
         {
            break;
         }
      }
      av_frame_free(&frame);
   }
   av_packet_free(&packet);
   return 0;
}
int VideoPlayer::DecodeThread(void* arg)
{
   VideoState* videoState = (VideoState*)arg;

   AVFormatContext* pFormatCtx = avformat_alloc_context();
   if (avformat_open_input(&pFormatCtx, videoState->fileName, NULL, NULL) < 0)
      return -1;
   videoState->pFormatCtx = pFormatCtx;
   if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
      return -1;
   av_dump_format(pFormatCtx, 0, videoState->fileName, 0);
   int videoIndex = -1;
   int audioIndex = -1;
   for (int i = 0; i < pFormatCtx->nb_streams; i++)
   {
      if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO && videoIndex < 0)
         videoIndex = i;
      if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO && audioIndex < 0)
         audioIndex = i;
   }
   if (audioIndex >= 0)
      instance().OpenStreamComponent(videoState, audioIndex);
   if (videoIndex >= 0)
      instance().OpenStreamComponent(videoState, videoIndex);
   if (videoState->videoThreadData->videoStreamInfo->videoStream < 0 || videoState->audioStreamInfo->audioStream < 0)
   {
      fprintf(stderr, "%s: could not open codecs\n", videoState->fileName);
      SDL_Event event;
      event.type = FF_QUIT_EVENT;
      event.user.data1 = videoState;
      SDL_PushEvent(&event);
      return 0;
   }
   AVPacket* packet = av_packet_alloc();
   int packetSize = 0;
   for (;;)
   {
      if (instance().m_quit)
         break;
      if (packetSize == 0 && av_read_frame(pFormatCtx, packet) >= 0)
         packetSize = packet->size;
      if (videoState->videoThreadData->videoq.size >= MAX_VIDEOQ_SIZE - packetSize ||
         videoState->audioq.size >= MAX_AUDIOQ_SIZE - packetSize)
      {
         SDL_Delay(100);
         continue;
      }
      if (packet->stream_index == videoState->videoThreadData->videoStreamInfo->videoStreamInd)
      {
         instance().PacketQueuePut(&videoState->videoThreadData->videoq, packet);
         packetSize = 0;
      }
      else if (packet->stream_index == videoState->audioStreamInfo->audioStreamInd)
      {
         instance().PacketQueuePut(&videoState->audioq, packet);
         packetSize = 0;
      }
      else
         packetSize = 0;
   }
   av_packet_free(&packet);
   while (!instance().m_quit) {
      SDL_Delay(100);
   }
   SDL_Event event;
   event.type = FF_QUIT_EVENT;
   event.user.data1 = videoState;
   SDL_PushEvent(&event);
   return 0;
}
VideoPlayer::VideoPlayer() 
{
   if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
   {
      fprintf(stderr, "Could not initialize SDL - %s\n", SDL_GetError());
      exit(1);
   }
   m_screen = SDL_CreateWindow("Testing..", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, \
      SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
   m_renderer = SDL_CreateRenderer(m_screen, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
   m_videoState = static_cast<VideoState*>(av_mallocz(sizeof(VideoState)));
   m_videoState->audioStreamInfo = static_cast<AudioStreamInfo*>(av_mallocz(sizeof(AudioStreamInfo)));
   m_videoState->audioBufferInfo = static_cast<AudioBufferInfo*>(av_mallocz(sizeof(AudioBufferInfo)));
   m_videoState->audioPacketInfo = static_cast<AudioPacketInfo*>(av_mallocz(sizeof(AudioPacketInfo)));
   m_videoState->frameTimerInfo = static_cast<FrameTimerInfo*>(av_mallocz(sizeof(FrameTimerInfo)));
   m_videoState->videoPts = static_cast<VideoPts*>(av_mallocz(sizeof(VideoPts)));
   m_videoState->videoThreadData = static_cast<VideoThreadData*>(av_mallocz(sizeof(VideoThreadData)));
   m_videoState->videoThreadData->videoStreamInfo = static_cast<VideoStreamInfo*>(av_mallocz(sizeof(VideoStreamInfo)));
   m_videoState->videoThreadData->pictQueueInfo = static_cast<PictQueueInfo*>(av_mallocz(sizeof(PictQueueInfo)));
   m_videoState->syncInfo = static_cast<SyncInfo*>(av_mallocz(sizeof(SyncInfo)));
   m_screenMutex = SDL_CreateMutex();
   m_videoState->videoThreadData->pictQueueInfo->pictQueueMutex = SDL_CreateMutex();
   m_videoState->videoThreadData->pictQueueInfo->pictQueueCondition = SDL_CreateCond();
   m_videoState->syncInfo->avSyncType = DEFAULT_AV_SYNC_TYPE;
   m_videoState->videoThreadData->videoStreamInfo->videoStreamInd = -1;
   m_videoState->audioStreamInfo->audioStreamInd = -1;
   ScheduleRefresh(m_videoState, 40);
}
int VideoPlayer::PlayVideo(char* fileName)
{
   SDL_Event event;
   strcpy_s(m_videoState->fileName, sizeof(m_videoState->fileName), fileName);
   m_videoState->parseThread = SDL_CreateThread(DecodeThread, (char*)m_videoState ,(void*)"123" );
   if (!m_videoState->parseThread)
   {
      return -1;
   }
   for (;;)
   {
      SDL_WaitEvent(&event);
      switch (event.type)
      {
      case FF_QUIT_EVENT:
      case SDL_QUIT:
         m_quit = 1;
         SDL_Quit();
         return 0;
         break;
      case FF_REFRESH_EVENT:
         VideoRefreshTimer(event.user.data1);
         break;
      default:
         break;
      }
   }
   return 0;
}
VideoPlayer::~VideoPlayer()
{
   av_free(m_videoState->syncInfo);
   av_free(m_videoState->videoThreadData->pictQueueInfo);
   av_free(m_videoState->videoThreadData->videoStreamInfo);
   av_free(m_videoState->videoThreadData);
   av_free(m_videoState->videoPts);
   av_free(m_videoState->audioPacketInfo);
   av_free(m_videoState->audioBufferInfo);
   av_free(m_videoState->audioStreamInfo);
   av_free(m_videoState);
}